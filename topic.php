<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" integrity="sha256-46qynGAkLSFpVbEBog43gvNhfrOj+BmwXdxFgVK/Kvc=" crossorigin="anonymous" />
    <title>Topic</title>

</head>
<body class="bg-light">

    <?php include('src/menu.php');
    
        $topic = new Topic($db);
        $comment = new Comment($db);
        $safety = new Validator($db);

        $topic_id = $_GET['id'];
        $rows = $topic->showTopicById($_GET['id']);

        if (isset($_POST["comments"])) {
            $comment->createComment(
               $safety->filter($_POST['response']), 
               $author = $_SESSION['data']['id'],
               $topic = $topic_id
            );
        }

    ?>

    <div class="container">

        <a class="btn btn-primary" href="javascript:history.go(-1)" role="button">Retour</a>

        <hr>

        <h1> <?php echo $rows['title']; ?> </h1>

        <p>
            <?php echo $rows['content']; ?>
        </p>

        <hr>

        <h3>Commentaires</h3>

        <?php 
            /* Peut être amélioré.. */
            $c = $comment->showComment($topic_id);
            foreach($c as $cs){

            $comment_id = $cs['id'];
            $a = $comment->showAuthor($comment_id);
        ?>

        <div class="media text-muted pt-3">
            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <a href="#"> <strong class="d-block text-gray-dark"><?php echo $safety->filter($a['pseudo']); ?></a>
                <?php echo $safety->filter($cs['content']); ?>
            </p>
        </div>

            <?php       
                }
            ?>

            <hr>

            <h3>Répondre</h3>
        <form action="#" method="post">
            <textarea class="form-control" id="response" name="response" rows="3"></textarea>

            <button class="btn btn-primary m-3" name="comments" type="submit">envoyer</button>
        </form>
        </div>



    </div>

    

    
</body>
</html>