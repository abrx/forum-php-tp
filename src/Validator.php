<?php
class Validator
{
    public static function isValidMail($var)
    {
        return filter_var($var, FILTER_VALIDATE_EMAIL);
    }

    public static function isLong($var, $max)
    {
        return(mb_strlen($var, 'UTF-8') > $max);
    }

    public static function isShort($var, $min)
    {
        return (mb_strlen($var, 'UTF-8') < $min);
    }

    public static function isValidID($var)
    {
        $var = (int)$var;
        return ($var > 0);
    }

    /*
     *
     * Fonction qui consiste à convertir tous les caractères éligibles en entités HTML
     * 
     */
    public function filter($var)
    {
        return htmlentities($var, ENT_QUOTES, 'UTF-8');
    }
}