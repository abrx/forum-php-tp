<?php

class Comment
{
    private $db = null;

    public function __construct(Database $db)
    {
       $this->db = $db;
    }

    public function createComment($content, $author, $topic)
    {
        $this->db->query("INSERT INTO comments(content, fk_user, fk_topic) VALUES(?, ?, ?)", [$content, $author, $topic]);
        echo '
        <div class="alert alert-success" role="alert">
            Votre commentaire est publié
        </div>';
    } 

    public function showComment($id)
    {
        $comment =  $this->db->query("SELECT * FROM comments WHERE fk_topic = ?", [$id], false);
        return $comment;
    }

    public function showAuthor($comment_id)
    {
        $author =  $this->db->query("SELECT pseudo FROM user JOIN comments ON (user.id = comments.fk_user) WHERE comments.id = ?", [$comment_id], true);
        return $author;
    }

    public function countCommentByAuthor($user_id)
    {
        $count =  $this->db->query("SELECT COUNT(*) FROM comments JOIN user ON (user.id = comments.fk_user) WHERE user.id = ?", [$user_id], true);
        return $count;
    }

}
