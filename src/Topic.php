<?php

class Topic
{
    private $db = null;

    public function __construct(Database $db)
    {
       $this->db = $db;
    }

    public function createTopic($title, $content, $author)
    {
        $this->db->query("INSERT INTO topic(title, content, fk_id_user) VALUES(?, ?, ?)", [$title, $content, $author]);
        echo '
        <div class="alert alert-success" role="alert">
            Topic publié
        </div>';
    }


    public function showTopicById($id)
    {
        $topic = $this->db->query("SELECT * FROM topic WHERE id = ? ", [$id], true);
        return $topic;
    }

    public function showTopic()
    {
        $topic =  $this->db->query("SELECT * FROM topic ORDER BY id DESC", [], false);
        return $topic;
    }

    public function countTopicByAuthor($user_id)
    {
        $count =  $this->db->query("SELECT COUNT(*) FROM topic JOIN user ON (user.id = topic.fk_id_user) WHERE user.id = ?", [$user_id], true);
        return $count;
    }

}
