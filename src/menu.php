<?php
session_start();

require 'src/Database.php';
require 'src/User.php';
require 'src/Validator.php';
require 'src/Topic.php';
require 'src/Comment.php';


$db = new Database();

if (!isset($_SESSION['logged'])) {
    header('Location: index.php');
}

if (isset($_POST["logout"])) {
    $user = new User($db);
    $user->logout();
}
?>
<nav class="navbar navbar-light bg-light shadow-sm">
		<a class="navbar-brand" href="themes.php">
    		<img src="img/f.svg" width="30" height="30" class="d-inline-block align-top" alt="">
  		</a>


        <ul class="nav justify-content-end">
            <li class="nav-item">
                <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="<?php echo $_SESSION['data']['pseudo']; ?>" href="profil.php"> <i class="fas fa-user"></i> </a>
            </li>

    <form action="" method="post">
            <li class="nav-item">
                <button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Déconnexion" type="submit" name="logout"> <i class="fas fa-sign-out-alt"></i> </button>
            </li>
    </form>

        </ul>
</nav>