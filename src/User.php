<?php
class User 
{

    private $db = null;
    private $errors = array();

    public function __construct(Database $db)
    {
       $this->db = $db;
    }

    /*
     *
     * Fonction qui consiste à créer un utilisateur 
     * 
     */
    public function createUser($pseudo, $password, $email)
    {
        if($this->validate($pseudo, $password, $email))
        {
            $pseudo = $this->filter($pseudo);
            $email = $this->filter($email);
            $exists = $this->exists($pseudo, $email);
            if(!$exists)
            {
                $password = $this->hash($password);

                $this->db->query("INSERT INTO user(email, password, pseudo) VALUES(?, ?, ?)", [$email, $password, $pseudo]);

                header('Location: profil.php');
                die();
            }
            else
            {
                $this->errors[] = 'Pseudo ou Email existe déjà';
            }
        }

        if(count($this->errors > 0))
        {
            $this->showErrors();
        }
    }


    /*
     *
     * Fonction qui consiste à connecter un utilisateur 
     * 
     */
    public function login($pseudo, $password)
    {
        if($this->validate($pseudo, $password))
        {
            $pseudo = $this->filter($pseudo);
            $exists = $this->exists2($pseudo, $password);
            if($exists)
            {
                session_start();
                $_SESSION['logged'] = 1;
                $_SESSION['data'] = $exists;

                header('Location: themes.php');
                die();
            }
            else
            {
                $this->errors[] = 'Mauvais identifiant ou mot de passe';
            }
        }

        if(count($this->errors > 0))
        {
            $this->showErrors();
        }
    }

    /*
     *
     * Fonction qui consiste à déconnecter un utilisateur 
     * 
     */
    public function logout()
    {
        session_start();  
	    session_destroy();  
        header('Location:index.php');
    }


    /*
     *
     * Fonction qui consiste à modifier un utilisateur 
     * 
     */
    public function updateUsers()
    {
        
    }








    /*
     *
     * Fonction qui consiste à vérifier si le pseudo et l'email sont en base de donnée
     * 
     */
    public function exists($pseudo, $email)
    {
        $exists =  $this->db->query("SELECT id FROM user WHERE pseudo = ? OR email = ? ", [$pseudo, $email], true);
        var_dump($exists);
        return ($exists > 0);
    }

    /*
     *
     * Fonction qui consiste à vérifier si le pseudo et le mot de passe sont en base de donnée
     * 
     */
    public function exists2($pseudo, $password)
    {
       $password = $this->hash($password);
       $exists =  $this->db->query("SELECT * FROM user WHERE pseudo = ? AND password = ? ", [$pseudo, $password], true);
       if($exists > 0) {
           return $exists; }
       return false;
    }

    /*
     *
     * Fonction qui consiste à valider les différents champs 
     * 
     */
    public function validate($pseudo, $password, $email = null)
    {
        if(Validator::isShort($pseudo, 4)) $this->errors[] = 'Pseudo : Minimum 4 caracteres';
        elseif(Validator::isLong($pseudo, 25)) $this->errors[] = 'Pseudo : Maximum 25 caracteres';

        if(Validator::isShort($password, 8)) $this->errors[] = 'Mot de passe : Minimum 8 caracteres';

        if($email !== null)
        {
            if(!Validator::isValidMail($email))
            {  
               $this->errors[] = 'Email invalide';
            }
        }
        return (count($this->errors) == 0);
    }

    /*
     *
     * Fonction qui consister à verifier si l'ID est valide
     * 
     */
    public function getUserData($id)
    {
        if(Validator::isValidID($id))
        {
            $rs =  $this->db->query("SELECT * FROM user WHERE id = ?", ['?'], true);
            if($rs == 1) return $rs->fetch(PDO::FETCH_ASSOC);
            return false;
        }
        return false;
    }

    /*
     *
     * Fonction qui consiste à chiffrer la variable passé en paramètre
     * 
     */
    private function hash($var)
    {
        return hash('sha512', $var);
    }

    /*
     *
     * Fonction qui consiste à convertir tous les caractères éligibles en entités HTML
     * 
     */
    public function filter($var)
    {
        return htmlentities($var, ENT_QUOTES, 'UTF-8');
    }


    /*
     *
     * Fonction qui consiste à afficher les erreurs de l'utilisateur
     * 
     */    
    private function showErrors()
    {
        foreach($this->errors as $e)
        {
            echo'<div class="alert alert-danger" role="alert">
                    '.$e.'
                </div>';
        }
    }








}