<?php

class Database
{
    
    private $db;

    public function __construct($host="localhost", $dbname="forum", $username="root", $password="root", $debug = true)
    {    
        try {
            $this->db = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
            $this->db->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_LOWER);
            $this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (\PDOException $error) {
            if ($debug) {
                die ($error->getMessage());
            } else {
                die ("Erreur base de donnée");
            }
        }

    }

    public function query($sth, $attr = [], $one = true)
    {
        $req = $this->db->prepare($sth);
        $res = $req->execute($attr);
        if (strpos($sth, "INSERT") === 0 || strpos($sth, "UPDATE") === 0 || strpos($sth, "DELETE") === 0) {
            return $res;
        }
        if ($one) {
            return $req->fetch();
        } else {
            return $req->fetchAll();
        }
    }

}
