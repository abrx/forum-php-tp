<?php
include('src/menu.php');

if (isset($_POST["insertTopic"])) {
    $topic = new Topic($db);
    $topic->createTopic(
       $_POST['title'], 
       $_POST['content'],
       $author = $_SESSION['data']['id'],
    );
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" integrity="sha256-46qynGAkLSFpVbEBog43gvNhfrOj+BmwXdxFgVK/Kvc=" crossorigin="anonymous" />
    <title>Topic</title>

</head>
<body class="bg-light">

    <div class="container">

        <form action="" method="post">

            <h2>Créer un topic</h2>
            <hr>
            <div class="form-group">
                <input class="form-control form-control-lg" type="text" name="title" placeholder="Titre">

                <textarea class="form-control mt-3" id="content" name=content rows="10" placeholder="Exprimez-vous, <?php echo $_SESSION['data']['pseudo']; ?> "></textarea>

                <button class="btn btn-primary m-3" type="submit" name="insertTopic">Poster</button>

            </div>

        </form>

    </div>

    

    
</body>
</html>