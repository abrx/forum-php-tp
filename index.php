<?php
require 'src/Database.php';
require 'src/User.php';
require 'src/Validator.php';

$db = new Database();

if (isset($_POST["register"])) {
    $user = new User($db);
    $user->createUser(
       $_POST['pseudo'], 
       $_POST['password'], 
       $_POST['email']
    );
}

if (isset($_POST["login"])) {
    $user = new User($db);
    $user->login(
       $_POST['pseudo'], 
       $_POST['password']
    );
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
    <title>Forum</title>
</head>
<body class="bg-light">

    <nav class="navbar navbar-light bg-light">
		<a class="navbar-brand" href="#">
    		<img src="img/f.svg" width="30" height="30" class="d-inline-block align-top" alt="">
  		</a>

        <form class="form-inline nav-item nav-link p-2 bd-highlight d-flex justify-content-end" action="" method="post">
            <div class="form-group mx-sm-3 mb-2">
                <input type="text" class="form-control form-control-sm" id="pseudo" name="pseudo" placeholder="Pseudo">
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <input type="password" class="form-control form-control-sm" id="password" name="password" placeholder="Mot de passe">
            </div>
            <button type="submit" name="login" class="btn btn-primary mb-2 btn-sm">Connexion</button>
        </form>
    </nav>


    <div class="container ">

		<div class="row d-flex justify-content-around">
			<div class="col-sm-4 col-sm-offset-2">
				<div class="col-sm-12 form-legend">
					<h2>S'inscrire</h2>
				</div>
				<div class="col-sm-12 form-column">

                    <!--FORMULAIRE D'INSCRIPTION-->
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="pseudo">Pseudo</label>
                            <input type="text" id="pseudo" name="pseudo" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Minimum 8 caractères">
                        </div>
                        <button type="submit" name="register" class="btn btn-primary">S'inscrire</button>
                    </form>


				</div>
			</div>

			<div class="row d-flex">
				<img src="img/home.svg" alt="" height="300px" whidth="250px">
			</div>

		</div>


    
</body>
</html>




