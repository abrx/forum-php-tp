<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" integrity="sha256-46qynGAkLSFpVbEBog43gvNhfrOj+BmwXdxFgVK/Kvc=" crossorigin="anonymous" />
    <title>Accueil</title>

</head>
<body class="bg-light">

<?php include('src/menu.php');

$topic = new Topic($db);

?>



<main role="main" class="container">

    <a class="btn btn-primary" href="create-topic.php" role="button">Créer un topic</a>

  <div class="my-3 p-3 bg-white rounded shadow-sm">
    <h6 class="border-bottom border-gray pb-2 mb-0"> <span class="badge badge-primary">All</span> | All topics</h6>

    <?php 
      $rows = $topic->showTopic();
      foreach($rows as $row){
    ?>

    <div class="media text-muted pt-3">
      <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
        <a href="topic.php?id=<?php echo $row['id']; ?>"> <strong class="d-block text-gray-dark"><?php echo $row['title']; ?></strong> </a>
        <?php echo substr($row['content'],0,400).'...'; ?>
      </p>
    </div>

    <?php       
        }     
    ?>

  </div>



</main>


    

    
</body>
</html>