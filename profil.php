<?php
session_start();


/* Peut être amélioré.. */
require 'src/Database.php';
require 'src/User.php';
require 'src/Validator.php';
require 'src/Topic.php';
require 'src/Comment.php';

$db = new Database();
$topic = new Topic($db);
$comment = new Comment($db);
$safety = new Validator($db);
/* Peut être amélioré.. */


if (!isset($_SESSION['logged'])) {
    header('Location: index.php');
}

if (isset($_POST["logout"])) {
    $user = new User($db);
    $user->logout();
}    

$user_id = $_SESSION['data']['id'];
$t = $topic->countTopicByAuthor($user_id);
$c = $comment->countCommentByAuthor($user_id);

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" integrity="sha256-46qynGAkLSFpVbEBog43gvNhfrOj+BmwXdxFgVK/Kvc=" crossorigin="anonymous" />
    <title>Profil</title>
</head>
<body class="bg-light">

    <nav class="navbar navbar-light bg-light shadow-sm">
		<a class="navbar-brand" href="themes.php">
    		<img src="img/f.svg" width="30" height="30" class="d-inline-block align-top" alt="">
  		</a>

        <ul class="nav justify-content-end">
            <li class="nav-item">
                <a class="nav-link" href="#"> <?php echo $_SESSION['data']['pseudo']; ?> </a>
            </li>

            <form action="" method="post">
                <li class="nav-item">
                    <button class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Déconnexion" type="submit" name="logout"> <i class="fas fa-sign-out-alt"></i> </button>
                </li>
            </form>

        </ul>
    </nav>

    
    <div class="container">

        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1>Mon profil</h1>
                <hr>
                <h2 class="display-4"><?php echo $_SESSION['data']['pseudo']; ?></h1>
                <p class="lead"><?php echo $_SESSION['data']['email']; ?></p>

                <!-- ajouter la fonction UPDATE user -->
                <button class="btn btn-primary" type="submit">Modifier mes informations</button>

                <hr>

                <div class="card" style="width: 18rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Topics publiés : <?php echo $safety->filter($t['count(*)']); ?> </li>
                        <li class="list-group-item">Commentaires publiés : <?php echo $safety->filter($c['count(*)']); ?> </li>
                    </ul>
                 </div>

            </div>
        </div>


    </div>

    

    
</body>
</html>